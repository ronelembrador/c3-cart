
// CSS Stylesheet
import './styles/App.css';

// Bootstrap
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

//NPM 
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { UserProvider } from './userContext';

// Pages 
import Home from './pages/Home';
import About from './pages/About';
import Product from './pages/Products';
import Signup from './pages/Signup';
import Login from './pages/Login';
import AppNavbar from './pages/AppNavbar';
import Footer from './pages/Footer';
import ProductDetail from './pages/ProductDetail';
import ForgotPassword from './pages/ForgotPassword';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import Cart from './pages/Cart'


export default function App(){

  const [ user, setUser ] = useState({
      id: null,
      firstname: null,
      lastname: null,
      isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear()    
  }

  useEffect(() => {
    let token = localStorage.getItem('token');
    fetch('https://gentle-dusk-18521.herokuapp.com/user/profile',{
      method: 'GET',
      headers: { Authorization: `Bearer ${token}`}
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          firstname: data.fullName[0].givenName,
          lastname: data.fullName[0].familyName,
          isAdmin: data.isAdmin
        })
      }else setUser({id: null, isAdmin: null})
    })
  }, []);

  return <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    	<AppNavbar/>
    	<Routes>
    		<Route exact path="/" element={<Home/>}/>
    		<Route exact path="/about" element={<About/>}/>
    		<Route exact path="/product" element={<Product/>}/>
    		<Route exact path="/signup" element={<Signup/>}/>
    		<Route exact path="/login" element={<Login/>}/>
        <Route exact path="/logout" element={<Logout/>}/>
        <Route exact path="/cart" element={<Cart/>}/>
        <Route exact path="/profile" element={<Profile/>}/>
        <Route exact path="/product-detail" element={<ProductDetail/>}/>
        <Route exact path="/forgot-password" element={<ForgotPassword/>}/>
    	</Routes>
    	<Footer/>
    </Router>
  </UserProvider>
}

