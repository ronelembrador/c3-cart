import '../styles/ProductDetail.css';
import { Fragment, useState } from 'react';
import { Row, Col, Button, Image } from 'react-bootstrap';
import Rating from './Rating';

export default function ProductDetail(){

	let [totalBought, setTotalBought] = useState(0)

	function addProduct(){
		setTotalBought(totalBought += 1)		
	}

	function lessenProduct(){
		if(totalBought > 0) setTotalBought(totalBought -= 1)		
	}

	return <Fragment>
		<div className="product-detail-container">
			<Row>
				<Col>
					<div className="product-detail__BoxImg">
						<Image src="https://images.unsplash.com/photo-1444731961956-751ed90465a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80" className="product-detail__img"/>
					</div>
				</Col>
				<Col>
					<div className="product-detail__content">
						<p className="product-detail__name">Cabbage</p>
						<div className="product-detail__rating">
							<p>Stars </p>
							<small>(209)</small>
						</div>
						<p className="product-detail__description">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
						<div className="product-detail__quantity">
							<p className="product-detail__quantity_title">Quantity</p>
							<Button size="sm" variant="light" className="product-detail__less" onClick={lessenProduct} disabled={totalBought > 0? false:true}>-</Button>
							<p className="product-detail__howMany">{totalBought}</p>
							<Button size="sm" variant="light" className="product-detail__more" onClick={addProduct}>+</Button>
						</div>
						<div className="product-detail__process">
							<Button size="sm" variant="light" className="product-detail__buyNowBtn">Buy Now</Button>	
							<Button size="sm" variant="light" className="product-detail__addToCartBtn">Add to Cart</Button>	
						</div>
					</div>
				</Col>
			</Row>
			<Rating/>
		</div>
	</Fragment>
}