import { Fragment, useState, useContext, useEffect } from 'react'; 
import { Form, Button } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import Container from '../components/Container';


export default function Cart() {

	const { user, setUser } = useContext(UserContext);
	const [ purchased, setPurchased ] = useState({
		productId: '',
		numberProductsBought: 0,
		purchasedOn: '',
		status: ''
	})
	let userInfo = [{}];
	let productId = []
	let numberProductsBought = []
	let purchasedOn = []
	let status = []

	useEffect(() => {

		let authToken = localStorage.getItem('token');

		fetch(`https://gentle-dusk-18521.herokuapp.com/user/profile`, {
			method: 'GET',
			headers: { Authorization: `Bearer ${authToken}`}
		})
		.then(res => res.json())
		.then(data => {

			fetch('')
			setPurchased(data.purchased.map((purchased,index) => {
				if(data.purchased.status === 'pending'){
				return <div key={index} className="cart-box">
					<p className="itemLabel">Item # {index}</p>
{/*{					<span>{address.lotNumber} {address.barangay} {address.city} {address.province} {address.country}</span>}*/}
				</div>
				}
			}))

			userInfo = data.purchased

			console.log(userInfo)

			userInfo.forEach(userPurchase => {

				if(userPurchase.status === 'pending') {
				productId.push(userPurchase.productId)
				numberProductsBought.push(userPurchase.numberProductsBought)
				purchasedOn.push(userPurchase.purchasedOn)
				status.push(userPurchase.status)
				} 
			})

		})


	}, [])

			console.log(userInfo)



	return<Fragment>
	<Container>
{/*	{purchased}*/}
	</Container>
	</Fragment>
}

