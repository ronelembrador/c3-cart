import '../styles/Home.css';
import { Fragment, useState } from 'react';
import Banner from './Banner';
import Cards from '../components/Cards';
import { Container, Row, Col, Button } from 'react-bootstrap';



export default function Home(){

	let [isLoaded, setIsLoaded] = useState(4);
	const CardDetails = [
		{
			image:"https://images.unsplash.com/photo-1605000797499-95a51c5269ae?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=871&q=80",
			title:"Cabbage",
		},
		{
			image:"https://images.unsplash.com/photo-1633966097253-70bb9abf1fe2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
			title:"Eggplant",
		},
		{
			image:"https://images.unsplash.com/photo-1625023725961-2a2b2d17e0c1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
			title:"Be Successful",
		},
		{
			image:"https://images.unsplash.com/photo-1625023725961-2a2b2d17e0c1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
			title:"Be Successful",
		},
		{
			image:"https://images.unsplash.com/photo-1625023725961-2a2b2d17e0c1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
			title:"Be Successful",
		},
		{
			image:"https://images.unsplash.com/photo-1625023725961-2a2b2d17e0c1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
			title:"Be Successful",
		},
		{
			image:"https://images.unsplash.com/photo-1625023725961-2a2b2d17e0c1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
			title:"Be Successful",
		},
	]

	function loadMore(){
		setIsLoaded(isLoaded += 4)
	}

	function loadLess(){
		setIsLoaded(isLoaded -= 4)
	}

	let Card = CardDetails.map((card,index) => {
		return <Col key={index}> 
			<Cards id={index} image={card.image} title={card.title} /> 
		</Col>

	})
	
	Card = Card.slice(0, isLoaded);
	
	return <Fragment>
		<Banner/>
		<Container className="products-container">
			<Row>
				{Card}
			</Row>
			{isLoaded <= Card.length? <Button variant="primary" onClick={loadMore} className="button-loadMore" size="sm">Load More</Button> : <Button variant="danger" onClick={loadLess} className="button-loadless" size="sm">show less</Button>}
		</Container>
	</Fragment>
}